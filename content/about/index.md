+++
date = "2016-05-03T18:56:57-04:00"
title = "About"
description = "About Jessica Huynh"
+++

Student of computer science, linguistics, and Arabic at Brandeis University. I’m currently bumbling along in the Language Based Spatial Reasoning lab doing research. I write things for the Internet too, apparently.

Heading, navigation, and copyright font is [Fira Sans](http://www.mozilla.org/en-US/styleguide/products/firefox-os/typeface/); monospace font is [Source Code Pro](https://github.com/adobe/Source-Code-Pro) and the default font is Source Sans Pro.
