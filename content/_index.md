+++
date = "2017-01-09T15:08:38-04:00"
title = "Jessica Huynh"
description = "Jessica Huynh's home page"
kind = "home"
url = "/"
+++

Student of computational linguistics at Brandeis University, research assistant in the Language Based Spatial Reasoning lab (part of the Communicating with Computers project)